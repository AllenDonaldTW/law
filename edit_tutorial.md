# gitlab 宪法编辑教程

首先，先注册一个 gitlab 账户，这应该不用教了吧。

然后打开 https://gitlab.com/simulating-china-ii/law/blob/master/constitution.md ，应该是这样的

![](edit_tutorial_1.png)

选择右上角的 Edit（中文版是编辑）

然后就可以编辑了

![](edit_tutorial_2.png)

然后提交 merge request，往下翻

![](edit_tutorial_3.png)

之后就提交好了。